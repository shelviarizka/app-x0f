package rizka.shelvia.appx0f

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

class MainActivity : AppCompatActivity() {
    var bundle: Bundle? = null
    var topik = "appx0f"
    var type = 0
    var token = "AAAAh9x4Ov8:APA91bHH2bY_uRt4mYG_bOnwS3MvAILaMsf47bMN0oD4AOo835Bu7kGHB6fi-vXLZB9l4TaFVUsinitUuEHB0EZHa5yfNodzWMjx-R3bY1QHmqS3YU3XN-XKIE5ZkUeviENeKtKQAj73"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        edToken.setText(token)
        FirebaseMessaging.getInstance().subscribeToTopic(topik)

    }

    override fun onResume() {
        super.onResume()

        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener(
                OnCompleteListener { task ->
                    if (task.isSuccessful) return@OnCompleteListener
                    edToken.setText(task.result!!.token)
                }
        )
        try {
            bundle = intent.extras
        } catch (e: Exception) {
            Log.e("BUNDLE", "bundle is null")
        }

        if (bundle !=null){
            type = bundle!!.getInt("type")
            when(type){
                0->{
                    edPromoId.setText(bundle!!.getString("promoId"))
                    edPromo.setText(bundle!!.getString("promo"))
                    edPromoUntil.setText(bundle!!.getString("promoUntil"))
                }
                1->{
                    edTitle.setText(bundle!!.getString("title"))
                    edBody.setText(bundle!!.getString("body"))
                }
            }
        }

    }

}




